from django.test import TestCase
from backend.models import User

# Create your tests here.
class SignInTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            email="email@test.com",
            password="password",
            first_name="John",
            last_name="Doe",
            sex=True,
            date_of_birth="1990-01-01",
            address="Jl. Test",
        )
        self.user.save()

    def tearDown(self):
        self.user.delete()
        return super().tearDown()

    def test_correct(self):
        response = self.client.post('/api/login', {
            'email': 'email@test.com',
            'password': 'password',
        })
        self.assertEqual(response.status_code, 200)
    
    def test_incorrect(self):
        response = self.client.post('/api/login', {
            'email': 'email@test.com',
            'password': 'wrong_password',
        })
        self.assertEqual(response.status_code, 401)

    def test_incorrect_email(self):
        response = self.client.post('/api/login', {
            'email': 'wrong_email@test.com',
            'password': 'password',
        })
        self.assertEqual(response.status_code, 401)