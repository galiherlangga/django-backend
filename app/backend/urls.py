from django.urls import path, include
from rest_framework import routers
from backend.views import auth, user

router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('register', auth.RegisterAPIView.as_view(), name='register'),
    path('update-password', auth.UpdatePasswordAPIView.as_view(), name='update-password'),
    path('user', user.UserAPIView.as_view(), name='users'),
    path('user/<int:pk>', user.UserDetailAPIView.as_view(), name='user-detail'),
]