from rest_framework import generics, permissions, status
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema

from backend.serializers import RegisterSerializer, UserSerializer, ResetPasswordSerializer
from backend.models import User

# Create your views here.
class RegisterAPIView(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    @swagger_auto_schema(responses={200: UserSerializer}, operation_description="Take sets of user data and create a new user.\nReturns the user data and a success message.\nUser created from this endpoint will be used as login credentials to get JWT token.")
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "message": "User Created Successfully.",
        })


class UpdatePasswordAPIView(generics.UpdateAPIView):
    serializer_class = ResetPasswordSerializer
    model = User
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    @swagger_auto_schema(responses={200: UserSerializer}, operation_description="Take old password, new password and confirm password and update the user password.\nReturns the user data and a success message.")
    def put(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response({"status": "success", "message": "Password has been updated successfully"}, status=status.HTTP_200_OK)