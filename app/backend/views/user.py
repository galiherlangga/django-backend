from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions, mixins, status
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema

from backend.models import User
from backend.serializers import UserSerializer, UserUpdateSerializer


class UserAPIView(generics.ListAPIView):
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(responses={200: UserSerializer}, operation_description="Get all users data.\nReturns the user data and a success message.\nAdditional query parameters can be used to filter the data.\nemail: filter by email\nfirst_name: filter by first name\nlast_name: filter by last name\nsex: filter by sex (0: male, 1: female)\naddress: filter by address\ndeleted: filter by deleted status (1: deleted, 0: show all, null: not deleted | default)", operation_id="get_users", operation_summary="Get all users data")
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = User.objects.all()
        email = self.request.query_params.get('email', None)
        if email is not None:
            queryset = queryset.filter(email__contains=email)
        first_name = self.request.query_params.get('first_name', None)
        if first_name is not None:
            queryset = queryset.filter(first_name__contains=first_name)
        last_name = self.request.query_params.get('last_name', None)
        if last_name is not None:
            queryset = queryset.filter(last_name__contains=last_name)
        sex = self.request.query_params.get('sex', None)
        if sex is not None:
            queryset = queryset.filter(sex=sex)
        address = self.request.query_params.get('address', None)
        if address is not None:
            queryset = queryset.filter(address__contains=address)
        deleted = self.request.query_params.get('deleted', None)
        # check if deleted value is true or false or null
        if deleted is not None:
            if deleted == '1':
                queryset = queryset.filter(deleted_at__isnull=False)
        else:
            queryset = queryset.filter(deleted_at__isnull=True)
        return queryset


class UserDetailAPIView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    serializer_class = None
    permission_classes = [permissions.IsAuthenticated]
    queryset = User.objects.all()

    @swagger_auto_schema(responses={200: UserSerializer}, operation_description="Get single user data by id. Returns the user data and a success message.", operation_id="get_user", operation_summary="Get single user data by id. Returns the user data and a success message.")
    def get(self, request, *args, **kwargs):
        self.serializer_class = UserSerializer
        return self.retrieve(request, *args, **kwargs)
    
    @swagger_auto_schema(responses={200: UserSerializer}, operation_description="Update user by id with new data. Data can be updated partialy. Returns the updated user data and a success message.", operation_id="update_user", operation_summary="Update user by id with new data. Data can be updated partialy. Returns the updated user data and a success message.")
    def put(self, request, *args, **kwargs):
        self.serializer_class = UserUpdateSerializer
        return self.update(request, *args, **kwargs)
    
    @swagger_auto_schema(responses={200: UserSerializer}, operation_description="Delete user by id. Return a success message.", operation_id="delete_user", operation_summary="Delete user by id. Return a success message.")
    def delete(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=kwargs['pk'])
        user.soft_deletes()
        return Response({"status":status.HTTP_204_NO_CONTENT, "message": "User deleted successfully"})
