# django-backend


## About
A django API project developed with docker environment
- app : Django
- db : MySql
- redis : Redis

## Getting started
Please run the following steps to run the project:
```
cp ./app/.env.example ./app/.env
docker-compose up -d --build
```

### API
To start using this API, please register new user on
```
http://localhost:8000/api/register
```
Then login using email and password from this step on
```
http://localhost:8000/api/login
```
This step will return access token that can be used as Bearer token

### Docs
Please access this link to see more about other API endpoints
```
http://localhost:8000/
or
http://localhost:8000/redoc
```